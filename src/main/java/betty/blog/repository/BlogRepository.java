package betty.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import betty.blog.entity.Blog;
import betty.blog.entity.User;

//Extends JpaRepository<referencedEntity, PrimaryKeyType> 
public interface BlogRepository extends JpaRepository<Blog, Integer>{

	List<Blog> findByCreatedBy(User user);
	
}
