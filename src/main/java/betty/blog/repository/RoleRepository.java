package betty.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import betty.blog.entity.Role;

//Extends JpaRepository<referencedEntity, PrimaryKeyType> 
public interface RoleRepository extends JpaRepository<Role, Integer>{

}
