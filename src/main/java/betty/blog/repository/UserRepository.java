package betty.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import betty.blog.entity.User;

//Extends JpaRepository<referencedEntity, PrimaryKeyType> 
public interface UserRepository extends JpaRepository<User, Integer>{

}
