package betty.blog.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import betty.blog.entity.Blog;
import betty.blog.entity.Item;

//Extends JpaRepository<referencedEntity, PrimaryKeyType> 
public interface ItemRepository extends JpaRepository<Item, Integer>{

	List<Item> findByBlog(Blog blog, Pageable pageable);

}
